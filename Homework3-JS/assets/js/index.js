// Constructor function for Song
function Song(title, artist, plays, duration) {
  this.title = title;
  this.artist = artist;
  this.plays = plays;
  this.duration = duration;
}

// Constructor function for MusicAlbum
function MusicAlbum(name, artist, artistImage, songs, yearReleased, albumCover, albumDuration) {
  this.name = name;
  this.artist = artist;
  this.artistImage = artistImage;
  this.songs = songs;
  this.yearReleased = yearReleased;
  this.albumCover = albumCover;
  this.albumDuration = albumDuration;

  // Method to add a song to the album - addSong e f-ja
  this.addSong = function(song) {
    this.songs.push(song);
  };

this.listAllSongs = function() { 
    const albumDiv = document.createElement('div');
    albumDiv.classList.add('album');
    albumDiv.innerHTML = `
      <img src="${this.albumCover}" alt="${this.name}" class="album-cover">
      <h3>${this.name} - ${this.artist} (${this.yearReleased}) - ${this.albumDuration}</h3>
    `;
    this.songs.forEach(song => {
      const songDiv = document.createElement('div');
      songDiv.classList.add('song');
      songDiv.innerHTML = `<img src="${this.artistImage}" alt="${this.name}" class="artist-img"> 
      ${song.title} - ${song.artist} - ${song.plays} - ${song.duration}`;
      albumDiv.appendChild(songDiv);
    });
    document.getElementById('albumList').appendChild(albumDiv);
  };
}

// Create a new album
const myAlbum = new MusicAlbum('B\'Day', 'Beyoncé', 'assets/img/song.png', [], 'September 4, 2006', 'assets/img/Beyonce_BDay.png', '1h 25min');

// Create a couple of songs
const song1 = new Song('Deja Vu', 'Beyoncé', "142,524,698", '3:30');
const song2 = new Song('Ring the Alarm', 'Beyoncé', "204,424,678", '3:23');
const song3 = new Song('Irreplaceable', 'Beyoncé', "2,854,698", '3:47');
const song4 = new Song('Beautiful Liar', 'Beyoncé', "7,112,345", '3:20');
const song5 = new Song('Get Me Bodied', 'Beyoncé', "10,985,100", '3:25');
const song6 = new Song('Green Light', 'Beyoncé', "5,214,965", '3:29')

// Add the songs to the album
myAlbum.addSong(song1);
myAlbum.addSong(song2);
myAlbum.addSong(song3);
myAlbum.addSong(song4);
myAlbum.addSong(song5);
myAlbum.addSong(song6);

// List all songs on the screen
myAlbum.listAllSongs();

