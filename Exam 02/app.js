function BookStore(name) {
  this.name = name;
  this.books = [];
  this.shoppingCard = [];
  this.wishlist = [];

  // addBook, listBooks, addToShoppingCard, addToWishlist, removeFromShoppingCard, removeFromWishlist

  this.addBook = function (book) {
    this.books.push(book);
  };

  this.listBooks = function () {
    const placeToListBooks = document.querySelector("#books-list");
    let htmlToAdd = "";
    for (let book of this.books) {
      htmlToAdd += `<li id="${book.id}" class="card">
        <div class="card-title">${book.title}</div>
        <div class="card-img"> <img src="${book.image}"></div>
        <div class="card-description">${book.description}</div>
        <div class="card-price"> Price: ${book.price} den.</div>
        <div class="card-quantity"> Quantity: ${book.quantity}</div>
        <div class="card-actions"> 
        <button class="buy-btn btn buy"> buy </button> 
        <button class="wishlist-btn btn"> add to wishlist </button>
        </div>
        </li>`;
    }
    placeToListBooks.innerHTML = htmlToAdd;
  };

  //addToShoppingCard
  this.addToShoppingCard = function (shoppingCard) {
    this.shoppingCard.push(shoppingCard);
  };

  //list shoppingCard
  this.listShoppingCard = function () {
    const placeToListShoppingCard = document.querySelector("#shopping-cart");
    let htmlToAdd = "";
    for (let shoppingC of this.shoppingCard) {
      htmlToAdd += `<li id="${shoppingC.id}">${shoppingC.title} ${shoppingC.author} ${shoppingC.price} 
    <a href="${shoppingC.image}" class="remove-book-cart" id="${shoppingC.id}">remove</a></li>`;
    }
    placeToListShoppingCard.innerHTML = htmlToAdd;
  };

  //addToWishlist
  this.addToWishlist = function (wishList) {
    this.wishlist.push(wishList);
  };

  this.listWishlist = function () {
    const placeToWish = document.querySelector("#wishlist");
    let htmlToAdd = "";
    for (let i of this.wishlist) {
      htmlToAdd += `<li id="${i.id}">${i.title} ${i.author} ${i.price} <a href="#" class="remove-book-wish" id="${i.id}">remove</a></li>`;
    }
    placeToWish.innerHTML = htmlToAdd;
  };

  this.removeFromShoppingCard = function (id) {
    const remShoppingCard = this.shoppingCard.find(
      (element) => element.id === id
    );
    if (remShoppingCard) {
      const positionCard = this.shoppingCard.indexOf(remShoppingCard);
      this.shoppingCard.splice(positionCard, 1);
      this.listShoppingCard();
    }
  };

  this.removeFromWishlist = function (id) {
    const remWishList = this.wishlist.find((element) => element.id === id);
    if (remWishList) {
      const positionWish = this.wishlist.indexOf(remWishList);
      this.wishlist.splice(positionWish, 1);
      this.listWishlist();
    }
  };
}

function Book(title, author, image, price, quantity, description) {
  this.id = Date.now();
  this.title = title;
  this.author = author;
  this.image = image;
  this.price = price;
  this.quantity = quantity;
  this.description = description;
}

const createBookStore = new BookStore("My Bookstore");

const buttonAdd = document.querySelector("#add-btn");
buttonAdd.addEventListener("click", function (e) {
  e.preventDefault();
  const title = document.querySelector("#title").value;
  const author = document.querySelector("#author").value;
  const image = document.querySelector("#thumbnail").value;
  const price = document.querySelector("#price").value;
  const quantity = document.querySelector("#quantity").value;
  const description = document.querySelector("#description").value;

  createBookStore.addBook(
    new Book(title, author, image, price, quantity, description)
  );
  createBookStore.listBooks();
});

document.addEventListener("click", function (e) {
  e.preventDefault();
  const elem = new Book(
    this.title,
    this.author,
    this.price,
    this.quantity,
    this.description
  );

  if (e.target.classList.contains("buy-btn")) {
    createBookStore.addToShoppingCard(elem);
    createBookStore.listShoppingCard();
  } else if (e.target.classList.contains("wishlist-btn")) {
    createBookStore.addToWishlist(elem);
    createBookStore.listWishlist();
  } else if (e.target.classList.contains("remove-book-cart")) {
    const cardIndex = parseInt(e.target.parentElement.id);
    createBookStore.removeFromShoppingCard(cardIndex);
  } else if (e.target.classList.contains("remove-book-wish")) {
    const wishIndex = parseInt(e.target.parentElement.id);
    createBookStore.removeFromWishlist(wishIndex);
  }
});
