//zemanje na vrednosta od input poleto - screen-ot
var screen = document.querySelector("#screen");
//zemanje na site kopcinja so klasa btn
var btn = document.querySelectorAll(".btn");

//zemanje na vrednosta na ostanatite kopcinja
var btnCE = document.querySelector("#ce");
var btnFactorial = document.querySelector("#factorial");
var btnAC = document.querySelector("#ac");
var btnSin = document.querySelector("#sin");
var btnPi = document.querySelector("#pi");
var btnCos = document.querySelector("#cos");
var btnLog = document.querySelector("#logarithm");
var btnTan = document.querySelector("#tan");
var btnSqrt = document.querySelector("#sqrt");
var btnE = document.querySelector("#e");
var btnPow = document.querySelector("#pow");
var btnEqual = document.querySelector("#equal");

// za da se zeme vrednosta od kopcinjata i deka se povekje iskoristeno e so for ciklus
//btn e kolekcija od povekje kopcinja i zatoa e staveno vo for
for (let i of btn) {
  i.addEventListener("click", (e) => {
    btntext = e.target.innerText;

    if (btntext == "×") {
      //uslov so proveruva dali kliknatoto kopce e so znak x, ako e togas mu ja dava f-jata mnozenje
      btntext = "*";
    }

    if (btntext == "÷") {
      //uslov so proveruva dali kliknatoto kopce e so znak ÷, ako e togas mu ja dava f-jata delenje
      btntext = "/";
    }
    screen.value += btntext;
  });
}

//f-ja za AC(all clear) kopce
function allClear() {
  screen.value = ""; //se resetira vrednosta na prazen string so cel da se izbrise se sho e napisano
}

//equal btn - f-jata eval se koristi za da moze da se ispisat expressions kako 1+2+3 itn
function equal() {
  try {
    screen.value = eval(screen.value); //ovaa linija kod e pisana vo try/catch bidejki ako stoi sama vo funkcijata se pojavuva greska vo konzola,
    // a so try/catch se pisuva error na ekranot od kalkulatorot koga nesto e pogresno ili nevozmozno da se presmeta ili e greska pisano/nelogicno. pr 6++6
  } catch (error) {
    screen.value = "Error";
  }
}

//f-ja za presmetuvanje sin
function sin() {
  screen.value = Math.sin(screen.value);
}

//f-ja za presmetuvanje cos
function cos() {
  screen.value = Math.cos(screen.value);
}

//f-ja za presmetuvanje tan
function tan() {
  screen.value = Math.tan(screen.value);
}

//f-ja za presmetuvanje kvadrat - bilo koj broj na stepen 2 (x^2)
function pow() {
  screen.value = Math.pow(screen.value, 2);
}

//f-ja za presmetuvanje kvadraten koren
function sqrt() {
  screen.value = Math.sqrt(screen.value, 2);
}

//f-ja za logaritam
function logarithm() {
  screen.value = Math.log(screen.value);
}

//f-ja za konstant pi
function pi() {
  screen.value = 3.14159265359;
}

//f-ja za konstanta e (eksponent)
function e() {
  screen.value = 2.71828182846;
}

// f-ja presmetuvanje na faktoriel
function fact() {
  let i, num;
  let f = 1;
  num = screen.value;
  for (i = 1; i <= num; i++) {
    f = f * i;
  }

  i = i - 1;

  screen.value = f;
}

//f-ja za kopceto CE(clear entry)
function clearEntry() {
  //substr e f-ja so koja se zadava rang kolku karakteri da se izbrisat od stringot.
  //Vo primerot e od pocetokot do krajot na stringot t.e se brise cel string koj e napisan, so toa sto se brise po 1 cifra odpozadi
  screen.value = screen.value.substr(0, screen.value.length - 1);
}

//povikuvanje na funkciite za kopcinjata
btnCE.addEventListener("click", clearEntry);
btnFactorial.addEventListener("click", fact);
btnAC.addEventListener("click", allClear);
btnSin.addEventListener("click", sin);
btnPi.addEventListener("click", pi);
btnCos.addEventListener("click", cos);
btnLog.addEventListener("click", logarithm);
btnTan.addEventListener("click", tan);
btnSqrt.addEventListener("click", sqrt);
btnE.addEventListener("click", e);
btnPow.addEventListener("click", pow);
btnEqual.addEventListener("click", equal);
