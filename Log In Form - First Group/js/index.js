var arrayUsers = ["user1", "user2", "user3", "user4"];
var arrayPasswords = ["password1!", "password2!", "password3!", "password4!"];

document.querySelector("#login-btn").addEventListener("click", function () {
  var username = document.querySelector("#username").value;
  var password = document.querySelector("#password").value;

  if (username === arrayUsers[0] && password === arrayPasswords[0]) {
    document.querySelector(
      "#message"
    ).textContent = `Successfully logged in! ${arrayUsers[0]}`;
  } else if (username === arrayUsers[1] && password === arrayPasswords[1]) {
    document.querySelector(
      "#message"
    ).textContent = `Successfully logged in! ${arrayUsers[1]}`;
  } else if (username === arrayUsers[2] && password === arrayPasswords[2]) {
    document.querySelector(
      "#message"
    ).textContent = `Successfully logged in! ${arrayUsers[2]}`;
  } else if (username === arrayUsers[3] && password === arrayPasswords[3]) {
    document.querySelector(
      "#message"
    ).textContent = `Successfully logged in! ${arrayUsers[3]}`;
  } else {
    document.querySelector(
      "#message"
    ).textContent = `Username does not exist or wrong password`;
  }
});
