function makeRecipe() {
  let recipeName = prompt("Vnesi ime na recept");
  let numberOfIngredients = Number(prompt("Vnesi broj na sostojki"));

  let ingredients = [];
  for (let i = 0; i < numberOfIngredients; i++) {
    let singleIngredient = prompt("Vnesi sostojka");
    ingredients.push(singleIngredient);
  }
  printRecipe(recipeName, numberOfIngredients, ingredients);
}

function printList(ingredients, hostElement) {
  let htmlAsString = "<ul>";
  for (let i = 0; i < ingredients.length; i++) {
    htmlAsString += `<li>${ingredients[i]}</li>`;
  }
  htmlAsString += "</ul>";

  hostElement.innerHTML += htmlAsString;
}

function printRecipe(recipeName, numberOfIngredients, ingredients) {
  const hostRecipe = document.querySelector(".host-recipe");

  let message = `<h2>${recipeName}</h2>
    <h3>Za podgotovka na ${recipeName} ke vi bidat potrebni ${numberOfIngredients} sostojki.</h3>
    `;

  hostRecipe.innerHTML = message;
  printList(ingredients, hostRecipe);
}

makeRecipe();
