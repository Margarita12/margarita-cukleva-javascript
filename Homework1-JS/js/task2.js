function printElements(){

    //Create an array with numbers
    var numbers = [1,2,3,4,5];

    //Print all numbers from the array in a list element, in a new HTML page
    var list = document.getElementById("list-id");


    var suma = 0;

    for(var i = 0; i < numbers.length; i++){
    
        var li = document.createElement("li");
        li.innerText = numbers[i];
        list.appendChild(li);

       //Print out the sum of all of the numbers below the list
        suma+=numbers[i];

    }
    
        //Print out the sum of all of the numbers below the list
        document.write("The sum of all of the numbers below the list is: "+suma+"<br>");


        //Bonus: Try printing the whole mathematical equasion as well ( 2 + 4 + 5 = 11)
        var bonus = numbers[0] + " + "+ numbers[1] + " + "+ numbers[2]+ " + " + numbers[3]+ " + "+numbers[4]+ " = "+suma;
        document.write("<br>"+bonus);
        
       
}

printElements();