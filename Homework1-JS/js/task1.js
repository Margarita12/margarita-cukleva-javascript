//Change the text for paragraphs tag and text tag
var paragraph1 = document.querySelector(".paragraph");
var paragraph2 = document.querySelector(".second");
var textElement = document.querySelector("text");

paragraph1.innerText = "This is paragraph 1";
paragraph2.innerText = "This is paragraph 2";
textElement.innerText = "Here is some new words for text element";

//Change the text of first h1 tag
var header1 = document.getElementById("myTitle");
header1.innerText = "First h1 tag";

//Change the text of second h1 tag
var secondH1 = document.getElementsByTagName("h1")[1];
secondH1.innerText = "Second h1 tag"

//Change the text of h3 tag
var header3 = document.getElementsByTagName("h3")[0];
header3.innerText = "This is header 3";
