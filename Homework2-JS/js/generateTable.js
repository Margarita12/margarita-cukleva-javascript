var row = document.querySelector("#rows");
var column = document.querySelector("#columns");
var generateBtn = document.querySelector("#generateBtn");
var tableOutput = document.querySelector("#generate-table");


function generateTable() {  
    var rowVal = row.value;  
    var columnVal = column.value; 

    let table = '<table>';  
    for (let i = 0; i< rowVal; i++) {  
    table += '<tr>';  
    for (let j = 0; j < columnVal; j++) {  
    table += `<td>Row ${i + 1} Column ${j + 1}</td>`;  
    }  
    table += '</tr>';  
    }  
    table += '</table>';  
    tableOutput.innerHTML = table;  
    } 

    generateBtn.addEventListener("click", function(){
        generateTable();
    });